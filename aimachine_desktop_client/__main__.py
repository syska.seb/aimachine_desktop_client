import sys

import pygame
from socketio import Client

from aimachine_desktop_client.board import Board
from aimachine_desktop_client.grid import Grid

HOST_URL = 'http://localhost:5000'
SOCKET = Client(reconnection=False)

BOARD = Board()
GRID = Grid()

GAME_ID = None
CLIENT_ID = None
MOVEMENT_ALLOWED = False

SCREEN = pygame.display.set_mode((600, 600))
pygame.init()
pygame.display.set_caption("Game")


@SOCKET.event
def connect():
    print("I'm connected!")


@SOCKET.event
def disconnect():
    print("I'm disconnected!")


@SOCKET.on('game_id')
def on_game_namespace(data):
    global GAME_ID
    GAME_ID = data
    print("game id: {}".format(GAME_ID))


@SOCKET.on('client_id')
def on_client_id(data):
    global CLIENT_ID
    CLIENT_ID = data
    print("client id: {}".format(CLIENT_ID))


@SOCKET.on('field_to_be_marked')
def on_field_to_be_marked(data):
    row_index = data["rowIndex"]
    col_index = data["colIndex"]
    field_token = data["fieldToken"]
    BOARD.set_field_value(row_index, col_index, field_token)
    print("field to be marked (row, col, token): {}, {}, {}".format(row_index, col_index, field_token))


@SOCKET.on('server_message')
def on_server_message(data):
    print("server message: {}".format(data))


@SOCKET.on('movement_allowed')
def on_movement_allowed(data):
    global MOVEMENT_ALLOWED
    MOVEMENT_ALLOWED = CLIENT_ID == data


SOCKET.connect(HOST_URL)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT or event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            SOCKET.disconnect()
            pygame.quit()
            sys.exit(0)
        if event.type == pygame.MOUSEBUTTONDOWN:
            if GAME_ID is not None and MOVEMENT_ALLOWED:
                if pygame.mouse.get_pressed()[0]:
                    pos = pygame.mouse.get_pos()
                    clicked_row_index, clicked_col_index = (pos[1] // 200, pos[0] // 200)
                    SOCKET.emit("field_clicked", {
                        "gameId": GAME_ID,
                        "rowIndex": clicked_row_index,
                        "colIndex": clicked_col_index})

    SCREEN.fill((0, 0, 0))
    GRID.draw_board(SCREEN, BOARD)
    pygame.display.flip()
