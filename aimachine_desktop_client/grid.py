from typing import Union

import numpy as np
import pygame
from pygame.surface import SurfaceType, Surface

from aimachine_desktop_client.board import Board
from aimachine_desktop_client.symbol import Symbol


class Grid:
    _IMAGE_X: Union[Surface, SurfaceType]
    _IMAGE_O: Union[Surface, SurfaceType]
    try:
        _IMAGE_X = pygame.image.load("resources/X.png")
        _IMAGE_O = pygame.image.load("resources/O.png")
    except FileNotFoundError:
        _IMAGE_X = pygame.image.load("../resources/X.png")
        _IMAGE_O = pygame.image.load("../resources/O.png")

    def __init__(self):
        self.grid_lines = [((0, 200), (600, 200)),
                           ((0, 400), (600, 400)),
                           ((200, 0), (200, 600)),
                           ((400, 0), (400, 600))]

        self.grid = [[0 for x in range(3)] for y in range(3)]

    def draw_board(self, screen: Union[pygame.Surface, SurfaceType], board: Board):
        for line in self.grid_lines:
            pygame.draw.line(screen, (200, 200, 200), line[0], line[1], 2)

        board_values = board.get_all_field_values()
        for col_index in range(np.size(board_values) // 3):
            for row_index in range(np.size(board_values) // 3):
                if board_values[row_index, col_index] == Symbol.SYMBOL_X.value.token:
                    screen.blit(self._IMAGE_X, (col_index * 200, row_index * 200))
                elif board_values[row_index, col_index] == Symbol.SYMBOL_O.value.token:
                    screen.blit(self._IMAGE_O, (col_index * 200, row_index * 200))
