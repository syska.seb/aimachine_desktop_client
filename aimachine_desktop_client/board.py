import numpy as np


class Board:
    BLANK_VALUE: int = 0

    def __init__(self, all_field_values: np.ndarray = None):
        if all_field_values is not None:
            self._all_field_values = all_field_values
        else:
            self._all_field_values = Board.BLANK_VALUE * np.zeros((3, 3), int)

    def get_all_field_values(self) -> np.ndarray:
        return self._all_field_values

    def set_field_value(self, row_index: int, col_index: int, field_value: int):
        self._all_field_values[row_index, col_index] = field_value
