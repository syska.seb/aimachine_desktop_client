from enum import Enum

from aimachine_desktop_client.attributes import Attributes


class Symbol(Enum):
    SYMBOL_X = Attributes(identifier="X", token=1)
    SYMBOL_O = Attributes(identifier="O", token=-1)
